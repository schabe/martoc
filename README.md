# martoc

martoc generates TOC files for audio CDs. It also converts your audio files,
normalizes the audio volume and extracts meta data to be used as
[CD-Text](https://en.wikipedia.org/wiki/CD-Text).

## Where to download

The official website, bug tracker etc. for martoc can be found at
[code.ott.net/martoc](https://code.ott.net/martoc/).

## How to use martoc

```
martoc -i /path/to/files/ -o /output/folder/
```

This command will process all audio files in `/path/to/files` and write its
output to `/output/folder`. The audio files will be normalized using the
`normalize` utility; if you would like to use some other tool for this job, use
the `-n` option:

```
martoc -i /path/to/files/ -o /output/folder/ -n wavegain
```

Afterwards you should find a bunch of .wav files plus one .toc file in your
output folder. If you want to check / modify the CD-Text data before burning,
the .toc file is where you can do just that.

When you are done, use something like this to burn the CD:

```
cdrdao write -v 2 --driver generic-mmc-raw --device /dev/sr0 out.toc
```

## Supported formats

* All audio formats supported by
  [FFmpeg](https://www.ffmpeg.org/general.html#Audio-Codecs)
* All meta data supported by [TagLib](https://taglib.org/)

## External dependencies

* [FFmpeg](https://www.ffmpeg.org/) to convert audio
* [TagLib](https://taglib.org/) to read tags
* [normalize](http://normalize.nongnu.org/) to normalize audio volume
* Something like [cdrdao](http://cdrdao.sourceforge.net/) to burn the CD

## License

martoc is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 3 of the License, or
(at your option) any later version.

This package is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this package. If not, see <http://www.gnu.org/licenses/>.
