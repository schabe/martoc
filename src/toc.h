#ifndef MARTOC_TOC_H
#define MARTOC_TOC_H

#include <list>

#include "audiofile.h"

class TOC
{
	std::list<AudioFile *> &tracks;
public:
	TOC(std::list<AudioFile *> &);
	void write(const std::string &fn);
};

#endif
