#include <dirent.h>
#include <sys/types.h>

#include <list>

#include "folder.h"
#include "toc.h"

MusicFolder::MusicFolder(const std::string dir)
{
	this->path = dir;
}

std::list<AudioFile *>
MusicFolder::find_tracks()
{
	DIR *dir;
	AudioFile *t;
	struct dirent *file;
	std::list<AudioFile *> tracks;

	dir = opendir(this->path.c_str());
	if (!dir)
	{
		perror(this->path.c_str());
		return tracks;
	}

	while ((file = readdir(dir)))
	{
		if (file->d_type == DT_DIR)
		{
			continue;
		}
		t = new AudioFile(this->path, file->d_name);
		tracks.push_back(t);
	}
	closedir(dir);

	tracks.sort(AudioFile::compare);

	return tracks;
}

int
MusicFolder::normalize(const std::string &dir, const std::string &normalize_cmd)
{
	std::string cmd;

	cmd = normalize_cmd + " " + dir;
	return system(cmd.c_str());
}

void
MusicFolder::convert_tracks(std::list<AudioFile *> tracks,
		const std::string &folder)
{
	std::list<AudioFile *>::iterator it;

	for (it = tracks.begin(); it != tracks.end(); ++it)
	{
		(*it)->convert(folder);
	}
}

void
MusicFolder::write_toc(std::list<AudioFile *> tracks, const std::string &dir_out)
{
	TOC toc(tracks);
	toc.write(dir_out + "/out.toc");
}

int
MusicFolder::process(const std::string &path_out, const std::string &normalize)
{
	std::list<AudioFile *> tracks;
	std::list<AudioFile *>::iterator it;

	tracks = this->find_tracks();
	this->convert_tracks(tracks, path_out);
	this->write_toc(tracks, path_out);

	for (it = tracks.begin(); it != tracks.end(); ++it)
	{
		delete *it;
	}

	return this->normalize(path_out + "/*.wav", normalize);
}
