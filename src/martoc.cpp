#include <iostream>
#include <unistd.h>

#include "config.h"
#include "folder.h"

using namespace std;

static void
help()
{
	cout << "Syntax: martoc [options]" << endl << endl;
	cout << "Options:" << endl;
	cout << "  -i DIR     Read original audio files from DIR" << endl;
	cout << "  -n CMD     Use CMD to normalize audio volume" << endl;
	cout << "  -o DIR     Write data to DIR" << endl;
	cout << endl;
	cout << "This is martoc " << PACKAGE_VERSION << endl;
	cout << "Report bugs at https://code.ott.net/martoc/" << endl;
}

int main(int argc, char **argv)
{
	int c;
	const char *dir, *outdir, *normalize;

	dir = ".";
	outdir = getenv("TMPDIR");
	normalize = "normalize";

	if (!outdir)
	{
		outdir = "/tmp/";
	}

	while ((c = getopt (argc, argv, "i:n:o:h")) != -1)
	{
		switch(c)
		{
		case 'i':
			dir = optarg;
			break;
		case 'n':
			normalize = optarg;
			break;
		case 'o':
			outdir = optarg;
			break;
		case 'h':
			help();
			return EXIT_SUCCESS;
		default:
			help();
			return EXIT_FAILURE;
		}
	}

	MusicFolder folder(dir);
	folder.process(outdir, normalize);
}
