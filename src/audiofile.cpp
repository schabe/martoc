#include <fstream>
#include <string.h>
#include <sys/stat.h>
#include <unistd.h>

#include "audiofile.h"

AudioFile::AudioFile(const std::string &dir, std::string path)
: folder(dir)
{
	this->fn = path;
	this->fn_wav = std::string(path + ".wav");
}

bool AudioFile::compare(const AudioFile *left, const AudioFile *right)
{
	return left->fn < right->fn;
}

bool AudioFile::load()
{
	std::string file;

	file = this->folder + "/" + this->fn;
	TagLib::FileRef f(file.c_str());
	TagLib::Tag *tag = f.tag();
	if (!tag)
	{
		std::cerr << this->fn << ": no tag found, skipping" << std::endl;
		return false;
	}
	this->artist = tag->artist();
	this->title = tag->title();

	return true;
}

void AudioFile::convert(const std::string &outdir)
{
	struct stat orig, wav;
	std::string infile, outfile;

	infile = std::string(this->folder) + "/" + this->fn;

	if (stat(infile.c_str(), &orig) == -1)
	{
		/* cannot read original file */
		throw std::runtime_error(infile + ": " + strerror(errno));
	}

	outfile = outdir + "/" + this->fn_wav;

	if (stat(outfile.c_str(), &wav) != -1)
	{
		/* both files found, check mtime */
		if (orig.st_mtim.tv_sec < wav.st_mtim.tv_sec)
		{
			std::cout << "Skipping " << this->fn << std::endl;
			return;
		}
	}

	std::cout << "Converting " << infile << std::endl;
	std::string cmd("ffmpeg -loglevel warning -y -i \"" + infile + "\" " +
		"-acodec pcm_s16le -ar 44100 -ac 2 " + outfile);

	if (system(cmd.c_str()) != 0)
	{
		unlink(outfile.c_str());
		throw std::runtime_error("Failed to convert file");
	}
}

void AudioFile::render(std::ofstream &out)
{
	out << std::endl;
	out << "TRACK AUDIO COPY" << std::endl;
	out << "CD_TEXT {" << std::endl;
	out << "  LANGUAGE 0 {" << std::endl;
	out << "    TITLE \"" << this->title << "\"" << std::endl;
	out << "    PERFORMER \"" << this->artist << "\"" << std::endl;
	out << "  }" << std::endl;
	out << "}" << std::endl;
	out << "AUDIOFILE \"" << this->fn_wav << "\" 0" << std::endl;
}
