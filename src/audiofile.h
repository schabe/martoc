#ifndef MARTOC_AUDIOFILE_H
#define MARTOC_AUDIOFILE_H

#include <iostream>
#include <string>

// taglib fileref
#include <fileref.h>

class AudioFile
{
	std::string fn_wav;
	TagLib::String artist;
	TagLib::String title;
	const std::string &folder;
	std::string fn;
public:
	AudioFile(const std::string &dir, std::string fn);
	bool load();
	void render(std::ofstream &out);
	void convert(const std::string &outdir);
	static bool compare(const AudioFile *, const AudioFile *);
};

#endif
