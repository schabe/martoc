#include <iostream>
#include <fstream>

#include "audiofile.h"
#include "toc.h"

TOC::TOC(std::list<AudioFile *> &t)
: tracks(t)
{
}

void
TOC::write(const std::string &fn)
{
	std::list<AudioFile *>::iterator it;
	std::ofstream out;

	out.open(fn, std::ios::out);
	out << "CD_DA" << std::endl << std::endl;
	out << "CD_TEXT {" << std::endl;
	out << "  LANGUAGE_MAP {" << std::endl;
	out << "    0 : EN" << std::endl;
	out << "  }" << std::endl;
	out << "  LANGUAGE 0 {" << std::endl;
	out << "    TITLE \"Music\"" << std::endl;
	out << "    PERFORMER \"Various Artists\"" << std::endl;
	out << "  }" << std::endl;
	out << "}" << std::endl;

	for (it = tracks.begin(); it != tracks.end(); ++it)
	{
		if ((*it)->load())
		{
			(*it)->render(out);
		}
	}
	out.close();
}
