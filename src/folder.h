#ifndef MARTOC_FOLDER_H
#define MARTOC_FOLDER_H

#include <string>
#include <list>

#include "audiofile.h"

class MusicFolder
{
	std::string path;
	std::list<AudioFile *> find_tracks();
	int normalize(const std::string &path, const std::string &cmd);
	void write_toc(std::list<AudioFile *>, const std::string &path);
	void convert_tracks(std::list<AudioFile *>, const std::string &path);
public:
	MusicFolder(const std::string path);
	int process(const std::string &path_out, const std::string &normalize);
};

#endif
